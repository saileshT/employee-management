package com.practice.employeemanagement.model.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import com.practice.employeemanagement.model.entity.EmployeeEntity;
import com.practice.employeemanagement.model.vo.EmployeeVO;

@Mapper(componentModel = "spring")
public interface EmployeeMapper {
	
	EmployeeVO convertEntityToVO(EmployeeEntity employeeEntity);

	EmployeeEntity convertVOToEntity(EmployeeVO employeeVO);

	List<EmployeeVO> convertEntityListToVOList(List<EmployeeEntity> employeeEntity);

	List<EmployeeEntity> convertVOListToEntityList(List<EmployeeVO> employeeVO);
}
